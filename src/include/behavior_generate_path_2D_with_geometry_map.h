/*!*******************************************************************************************
 *  \brief      Behavior Generate path 2D with geometry map implementation file.
 *  \details    This behavior uses trajectory planner, path tracker, 
 *              obstacle detector and quadrotor pid controller
 *  \authors    Alberto Rodelgo Perales, Rafael Artiñano
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef GENERATE_PATH_H
#define GENERATE_PATH_H
// System
#include <string>
#include <iostream>
#include <tuple>
#include <map>
#include <yaml-cpp/yaml.h>
#include <signal.h>
#include <sstream>
#include <iomanip>
#include <nodelet/nodelet.h>
#include <behavior_execution_controller.h>
// ROS
#include <ros/ros.h>

//Msgs
#include <aerostack_msgs/BehaviorEvent.h>
#include <std_msgs/Bool.h>
#include "std_srvs/Empty.h"
#include <droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/AddBelief.h>
#include <droneMsgsROS/RemoveBelief.h>
#include <boost/range/combine.hpp>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <boost/algorithm/string/replace.hpp>
#include <droneMsgsROS/AddBelief.h>
#include <droneMsgsROS/GenerateID.h>
#include <droneMsgsROS/ConsultIncompatibleBehaviors.h>
#include <aerostack_msgs/BehaviorCommand.h>
#include <aerostack_msgs/RequestBehavior.h>
#include <aerostack_msgs/RequestProcesses.h>
#include <droneMsgsROS/dronePose.h>
#include <tuple>

namespace navigation_with_geometry_map
{
class BehaviorGeneratePath2DWithGeometryMap : public BehaviorExecutionController
{
  // Constructor
public:
  BehaviorGeneratePath2DWithGeometryMap();
  ~BehaviorGeneratePath2DWithGeometryMap();

private:
  ros::NodeHandle node_handle;
  std::string nspace; 
  
  bool finished;
  bool first=0;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;


  std::string add_belief_id;
  bool path_generated=false;
  bool is_finished;
  ros::Publisher mission_point_pub;
  ros::Subscriber trajectory_generated_sub;
  ros::ServiceClient request_processes_activation_cli;
  ros::ServiceClient request_processes_deactivation_cli;  
  ros::ServiceClient id_gen_client;  
  ros::ServiceClient add_belief_service;
  droneMsgsROS::dronePose target_position;
  droneMsgsROS::dronePose estimated_pose_msg;

  aerostack_msgs::RequestProcesses request_processes_srv;
private:
  // BehaviorExecutionController
  void onConfigure();
  void onActivate();
  void onDeactivate();
  void onExecute();
  bool checkSituation();
  void checkGoal();
  void checkProgress();
  void checkProcesses();


  //void ownSetUp();
  //void ownStart();
  //void ownRun();
  //void ownStop();
  int requestBeliefId();
  void trajectory_callback(const droneMsgsROS::dronePositionTrajectoryRefCommand &);
  void estimatedPoseCallBack(const droneMsgsROS::dronePose&);
};
}

#endif
