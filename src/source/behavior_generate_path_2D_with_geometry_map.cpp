/*!*******************************************************************************************
 *  \brief      Behavior Generate path 2D with geometry map implementation file.
 *  \details    This behavior uses trajectory planner, path tracker, 
 *              obstacle detector and quadrotor pid controller
 *  \authors    Alberto Rodelgo Perales, Rafael Artiñano
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_generate_path_2D_with_geometry_map.h"
#include <pluginlib/class_list_macros.h>

namespace navigation_with_geometry_map
{
BehaviorGeneratePath2DWithGeometryMap::BehaviorGeneratePath2DWithGeometryMap() : BehaviorExecutionController() { 
  setName("generate_path_2d_with_geometry_map");
  setExecutionGoal(ExecutionGoals::ACHIEVE_GOAL);
}

BehaviorGeneratePath2DWithGeometryMap::~BehaviorGeneratePath2DWithGeometryMap() {}

void BehaviorGeneratePath2DWithGeometryMap::onConfigure()
{
  node_handle = getNodeHandle();
  nspace = getNamespace();
}

bool BehaviorGeneratePath2DWithGeometryMap::checkSituation()
{
return true;
}

void BehaviorGeneratePath2DWithGeometryMap::checkGoal(){ 
    if(is_finished==false)
    {
      if(path_generated)
      {
        BehaviorExecutionController::setTerminationCause(aerostack_msgs::BehaviorActivationFinished::GOAL_ACHIEVED);
        is_finished = true;
        return;
      }
    }
}

void BehaviorGeneratePath2DWithGeometryMap::onExecute()
{
  
}

void BehaviorGeneratePath2DWithGeometryMap::checkProgress() {

}

void BehaviorGeneratePath2DWithGeometryMap::onActivate()
{
  //Processes
  ros::ServiceClient start_controller=node_handle.serviceClient<std_srvs::Empty>("/"+nspace+"/droneTrajectoryPlanner/start");
  std_srvs::Empty req;
  start_controller.call(req);

  //Topics
  mission_point_pub = node_handle.advertise<droneMsgsROS::dronePositionRefCommand>("/" + nspace + "/droneMissionPoint",1);
  trajectory_generated_sub = node_handle.subscribe("/" + nspace + "/droneTrajectoryAbsRefGenerated",1,&BehaviorGeneratePath2DWithGeometryMap::trajectory_callback,this);
  add_belief_service = node_handle.serviceClient<droneMsgsROS::AddBelief>("/" + nspace + "/add_belief");
  id_gen_client = node_handle.serviceClient<droneMsgsROS::GenerateID>("/" + nspace + "/" + "belief_manager_process/generate_id");  

  //Waiting time
  sleep(1);
  
  first=0;
  path_generated=false;
  is_finished=false;
  std::string arguments = getParameters();
  YAML::Node config_file = YAML::Load(arguments);
  if(config_file["destination"].IsDefined()){
    std::vector<double> points=config_file["destination"].as<std::vector<double>>();
    target_position.x=points[0];
    target_position.y=points[1];
    target_position.z=points[2];
  }else{
      //setStarted(false);
      return;
  }
  droneMsgsROS::dronePositionRefCommand mission_point;
  mission_point.x=estimated_pose_msg.x;
  mission_point.y=estimated_pose_msg.y;
  mission_point.z=estimated_pose_msg.z;
  mission_point_pub.publish(mission_point);
  mission_point.x=target_position.x;
  mission_point.y=target_position.y;
  mission_point.z=target_position.z;
  mission_point_pub.publish(mission_point);
}

void BehaviorGeneratePath2DWithGeometryMap::onDeactivate()
{
  //Shutdown topics
  //trajectory_generated_sub.shutdown();
  add_belief_service.shutdown();
  id_gen_client.shutdown();
  mission_point_pub.shutdown();

  //Stop processes
  ros::ServiceClient stop_controller=node_handle.serviceClient<std_srvs::Empty>("/"+nspace+"/droneTrajectoryPlanner/stop");
  std_srvs::Empty req;
  stop_controller.call(req);
}

void BehaviorGeneratePath2DWithGeometryMap::checkProcesses() 
{ 
 
}


// Callbacks

void BehaviorGeneratePath2DWithGeometryMap::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
  estimated_pose_msg=msg;
}

void BehaviorGeneratePath2DWithGeometryMap::trajectory_callback(const droneMsgsROS::dronePositionTrajectoryRefCommand & msg)
{
  if(!path_generated && msg.droneTrajectory[msg.droneTrajectory.size()-1].x==(long)target_position.x && msg.droneTrajectory[msg.droneTrajectory.size()-1].y==(long)target_position.y && msg.droneTrajectory[msg.droneTrajectory.size()-1].z==(long)target_position.z){
    droneMsgsROS::AddBelief add_belief_msg;
    std::string str;
    std::string belief_object;
    add_belief_id = std::to_string(requestBeliefId());
    belief_object = "object(" + add_belief_id + ", path)";
    str =  "path("+add_belief_id+",(";
    for(int i=0;i<msg.droneTrajectory.size();i++){
      if(i==0){
        std::stringstream osx ;
        osx << std::setprecision(3) << std::fixed << msg.droneTrajectory[i].x;
        std::stringstream osy ;
        osy << std::setprecision(3) << std::fixed << msg.droneTrajectory[i].y;
        std::stringstream osz ;
        osz << std::setprecision(3) << std::fixed << msg.droneTrajectory[i].z;
      std::string ss="( " +osx.str()+", "+ osy.str()+", "+osz.str()+" )";
      str=str + ss;}
      else{
        std::stringstream osx ;
        osx << std::setprecision(3) << std::fixed << msg.droneTrajectory[i].x;
        std::stringstream osy ;
        osy << std::setprecision(3) << std::fixed << msg.droneTrajectory[i].y;
        std::stringstream osz ;
        osz << std::setprecision(3) << std::fixed << msg.droneTrajectory[i].z;
        std::string ss=", ( " +osx.str()+", "+ osy.str()+", "+osz.str()+" )";
        str=str +ss;
      }
    }
    str=str+"))";
    std::cout<< str <<std::endl;
    add_belief_msg.request.multivalued = false;
    //Object
    add_belief_msg.request.belief_expression = belief_object;
    add_belief_service.call(add_belief_msg);       
    //Path
    add_belief_msg.request.belief_expression = str;
    add_belief_service.call(add_belief_msg); 
    path_generated=true;
  }
}

int BehaviorGeneratePath2DWithGeometryMap::requestBeliefId()
{
  int ret = 100;
  droneMsgsROS::GenerateID::Request req;
  droneMsgsROS::GenerateID::Response res;
  
  id_gen_client.call(req, res);
  if (res.ack)
  {
    ret = res.id;
  }
  return ret;
}

}
PLUGINLIB_EXPORT_CLASS(navigation_with_geometry_map::BehaviorGeneratePath2DWithGeometryMap, nodelet::Nodelet)