# Navigation with geometry map

## Behavior `GENERATE_PATH_2D_WITH_GEOMETRY_MAP`

| Arguments    |   Format  |  Example |  
| :-----------| :---------| :--------|
| destination | Tuple of 3 numbers x, y, z (meters)| destination: [3.3,3.3,0.7]|

**Description:** This behavior generates a path free of obstacles between the current position of the robot and the given destination. This behavior uses a representation of the environment as a geometric map (with cylinders, cuboids, etc.). The result of this task is stored in the belief memory in the predicates: object(X, path) and path(X, Y), where Y is a list of 3D points. For example: object(32, path), path(32, ((1.1,1.1,0.7), (2.2,2.2,0.7), (3.3,3.3,0.7))). Note that the height value of each point (z value) is a constant value because the path is planned on a horizontal surface.

----


# Contributors

**Code maintainer:** Alberto Rodelgo Perales

**Authors:** Alberto Rodelgo Perales, Rafael Artiñano